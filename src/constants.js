export const STATUS_ERROR = 'error';
export const STATUS_PENDING = 'pending';
export const STATUS_OK = 'ok';

export const ACTION_DISMISS_ERROR = 'user/dismiss-error';
export const ACTION_LOGIN = 'user/login';
export const ACTION_REGISTER = 'user/register';

export const ACTION_UPDATE_PROFILE = 'user/update-profile';

export const ACTION_LIST_SONGS = 'user/list-songs';
export const ACTION_CREATE_JAM = 'user/create-jam';
export const ACTION_LIST_JAMS = 'user/list-jams';
export const ACTION_JOIN_JAM = 'user/join-jam';
export const ACTION_VIEW_JAM = 'user/view-jam';
export const ACTION_LOAD_JAM = 'user/load-jam';
export const ACTION_DISMISS_JAM = 'user/dismiss-jam';
export const ACTION_START_JAM = 'user/start-jam';
/**
 * Create a new subscription
 * @type {string}
 */
export const ACTION_SUBCRIBE = 'user/subscribe';
export const ACTION_PUSH_JAM_CREATED = 'push/jam-created'; // payload should include changeset
export const ACTION_PUSH_JAM_UPDATED = 'push/jam-updated'; // payload should include changeset
