/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useContext, useEffect } from 'react';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import {
  ACTION_DISMISS_JAM,
  ACTION_LOAD_JAM,
  ACTION_START_JAM,
  STATUS_ERROR,
  STATUS_OK,
  STATUS_PENDING
} from "../constants";
import { DispatchContext, StateContext } from "./context";
import network from "../network";

export default () => {
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  useEffect(() => {
    (async () => {
      const jamId = state.get('activeJamId');
      dispatch({ type: ACTION_LOAD_JAM, status: STATUS_PENDING, jamId });
      try {
        const { ok, error, jam } = await network.get(`/api/jams/${jamId}`);
        if (ok) {
          dispatch({ type: ACTION_LOAD_JAM, status: STATUS_OK, jam });
          return;
        }
        dispatch({ type: ACTION_LOAD_JAM, status: STATUS_ERROR, error });
      } catch (error) {
        dispatch({ type: ACTION_LOAD_JAM, status: STATUS_ERROR, error });
      }
    })()
  }, []);

  const onStartJamClick = useCallback(async (jamId) => {
    try {
      const { ok, jam, error } = await network.post(`/api/jams/${jamId}/sessions`, {});
      if (ok) {
        dispatch({ type: ACTION_START_JAM, status: STATUS_OK, jam });
        return;
      } else {
        dispatch({ type: ACTION_START_JAM, status: STATUS_ERROR, error });
        return;
      }
    } catch (error) {
      dispatch({ type: ACTION_START_JAM, status: STATUS_ERROR, error });
    }
  }, [dispatch, state]);
  const onToggle = () => dispatch({ type: ACTION_DISMISS_JAM });

  const activeJamId = state.get('activeJamId');
  const jam = state.get('jams').find(item => item.get('_id') === activeJamId);

  if (!jam) {
    return (
    <Modal fade isOpen>
      <ModalBody>
        <p className="text-center">Loading...</p>
      </ModalBody>
    </Modal>);
  }

  return (
    <Modal fade isOpen>
      <ModalHeader toggle={onToggle}>
        {jam.get('status') === 'PENDING' && <span>Waiting for performers...</span>}
        {jam.get('status') === 'READY' && <span>Waiting for host to start...</span>}
        {jam.get('status') === 'INPROG' && <span>You are now jamming...</span>}
      </ModalHeader>
      {!!jam &&
      <ModalBody>
        <h6>Players</h6>
        {!!(jam.get('playerProfiles') && jam.get('playerProfiles').size) &&
        <ul className="list-group list-group-flush">
          {jam.get('playerProfiles').map(item => (
            <li key={item.get('_id')} className="list-group-item">
              {item.get('username')}
              <span className="float-right">
              {item.get('instrument')}
            </span>
            </li>
          ))}
        </ul>
        }
      </ModalBody>
      }
      {!!jam &&
      <ModalFooter>
        <h6 className="mr-auto">
          {jam.get('title')}
          <span className="mt-1 badge badge-pill badge-dark ml-2">
            {jam.get('status')}
          </span>
        </h6>
        {jam.get('owned') && jam.get('status') !== 'INPROG' &&
        <button
          className="btn btn-primary"
          disabled={jam.get('status') !== 'READY'}
          onClick={() => onStartJamClick(jam.get('_id'))}
        >Start</button>
        }
        {jam.get('owned') && jam.get('status') === 'INPROG' &&
        <button
          className="btn btn-danger"
        >Stop</button>
        }
        {!jam.get('owned') && jam.get('status') === 'INPROG' &&
        <button
          className="btn btn-danger"
        >Leave</button>
        }
      </ModalFooter>
      }
      {!jam &&
      <ModalBody>Ooops!</ModalBody>
      }
    </Modal>
  );
};
