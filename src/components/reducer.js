import {
  ACTION_LIST_SONGS,
  STATUS_ERROR,
  ACTION_LOGIN,
  ACTION_DISMISS_ERROR,
  ACTION_LIST_JAMS,
  STATUS_PENDING,
  ACTION_CREATE_JAM,
  ACTION_VIEW_JAM,
  ACTION_DISMISS_JAM,
  ACTION_LOAD_JAM,
  ACTION_PUSH_JAM_UPDATED,
  ACTION_PUSH_JAM_CREATED,
  ACTION_JOIN_JAM,
} from '../constants';
import { fromJS } from "immutable";

export default (state, action) => {
  console.log({ state, action });
  if (action.status === STATUS_PENDING) {
    return state;
  }

  if (action.status === STATUS_ERROR) {
    console.error({ error: action.error });
    return state.set('errorText', action.error.toString());
  }

  const userId = state.getIn(['profile', '_id']);

  switch (action.type) {
    case ACTION_DISMISS_ERROR: {
      return state.set('errorText', null);
    }
    case ACTION_LOGIN: {
      const { profile } = action;
      return state
          .set('profile', fromJS({
            ...profile,
            isLoggedIn: true,
          }))
          .updateIn(['jams'], (jams) => jams.map(item => {
            return item.set('joined', item.get('players').includes(profile._id));
          }));
    }
    case ACTION_LIST_SONGS: {
      return state.set('songs', fromJS(action.songs));
    }
    case ACTION_LIST_JAMS: {
      return state.set('jams', fromJS(action.jams.map(item => ({
        ...item,
        owned: userId === item.ownerId,
        joined: item.players.includes(userId),
      }))));
    }
    case ACTION_CREATE_JAM: {
      return state.updateIn(['jams'], (jams) => jams.unshift(fromJS(
        {
          ...action.jam,
          joined: true,
          owned: true,
        })));
    }
    case ACTION_VIEW_JAM: {
      return state.set('activeJamId', action.jamId);
    }
    case ACTION_LOAD_JAM: {
      return state.updateIn(['jams'], (jams) => jams.map(item => {
        if (item.get('_id') === action.jam._id) {
          return item.merge(fromJS(action.jam));
        }
        return item;
      }));
    }
    case ACTION_DISMISS_JAM: {
      return state.set('activeJamId', null);
    }
    case ACTION_PUSH_JAM_CREATED: {
      return state.updateIn(['jams'], (jams) => jams.unshift(fromJS({
        ...action.jam,
        owned: userId === action.jam.ownerId,
        joined: action.jam.players.includes(userId),
      })));
    }
    case ACTION_JOIN_JAM: {
      return state
          .set('activeJamId', action.jam._id)
          .updateIn(['jams'], (jams) => jams.map(item => {
            if (item.get('_id') === action.jam._id) {
              return item.merge(fromJS({
                ...action.jam,
                joined: action.jam.players.includes(userId),
              }));
            }
            return item;
          }));
    }
    case ACTION_PUSH_JAM_UPDATED: {
      return state.updateIn(['jams'], (jams) => jams.map(item => {
        if (item.get('_id') === action.jam._id) {
          return item.merge(fromJS({
            ...action.jam,
            joined: action.jam.players.includes(userId),
          }));
        }
        return item;
      }));
    }
    default: return state;
  }
}
