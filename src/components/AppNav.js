/* eslint-disable jsx-a11y/anchor-is-valid,no-script-url */
import React from 'react';
import { NavLink } from 'react-router-dom';

export default ({ profile }) => (
  <nav className="navbar navbar-expand navbar-light bg-light">
    <a className="navbar-brand">MJ</a>

    <ul className="navbar-nav mx-auto">
      <li className="nav-item">
        <NavLink exact className="nav-link" to="/songs">Songs</NavLink>
      </li>
      <li className="nav-item">
        <NavLink exact className="nav-link" to="/jams">Jams</NavLink>
      </li>
    </ul>
    <ul className="navbar-nav mr-0">
      <li className="nav-item text-uppercase">
        <span className="nav-link font-weight-bold">{profile.get('username')}</span>
      </li>
      <li className="nav-item">
        <NavLink exact className="nav-link" to="/profile">
          My Profile
        </NavLink>
      </li>
    </ul>
  </nav>
);
