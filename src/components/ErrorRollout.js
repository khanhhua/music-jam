import React, { useContext } from 'react';
import { DispatchContext } from "./context";
import { ACTION_DISMISS_ERROR } from "../constants";

export default ({ visible, errorText }) => {
  const dispatch = useContext(DispatchContext);

  return (
    <div
      className={`w-100 position-absolute alert alert-warning fade ${visible ? 'show' : ''}`}
      style={{ zIndex: 9999, top: 0, display: visible ? 'block' : 'none' }}
    >
      {errorText}

      <button
        type="button"
        className="close"
        aria-label="Close"
        onClick={() => dispatch({ type: ACTION_DISMISS_ERROR })}
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
};
