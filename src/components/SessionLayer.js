/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from 'react';
import { DispatchContext, StateContext } from "./context";
import network from "../network";
import {
  ACTION_LOGIN,
  ACTION_PUSH_JAM_CREATED,
  ACTION_PUSH_JAM_UPDATED,
  ACTION_SUBCRIBE,
  STATUS_ERROR,
  STATUS_OK
} from "../constants";

const WSS_BASE_URL = process.env.REACT_APP_WSS_BASE_URL || 'ws://localhost:8000';

export default ({ children }) => {
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  const [ws, setWS] = useState(null);

  useEffect(() => {
    (async () => {
      if (window.location.hash.indexOf('/login') !== -1
          || window.location.hash.indexOf('/register') !== -1) {
        // No session is required
        return;
      }
      const tokenExists = !!localStorage.getItem('mj:token');
      if (!state.getIn(['profile', 'isLoggedIn']) && tokenExists) {
        const { ok, profile } = await network.get('/api/profiles/me');
        if (!ok) {
          localStorage.clear();
          window.location.href = '/#/login';
          return;
        }
        dispatch({ type: ACTION_LOGIN, status: STATUS_OK, profile });
      } else if (state.getIn(['profile', 'isLoggedIn'])) {
        const { ok, profile } = await network.get('/api/profiles/me');
        if (!ok) {
          localStorage.clear();
          window.location.href = '/#/login';
          return;
        }
        dispatch({ type: ACTION_LOGIN, status: STATUS_OK, profile });
      } else {
        localStorage.clear();
        window.location.href = '/#/login';
      }
    })();
  }, [state.getIn(['profile', 'isLoggedIn'])]);
  useEffect(() => {
    (async () => {
      if (ws) {
        return;
      }

      if (!state.getIn(['profile', 'isLoggedIn'])) {
        return;
      }
      try {
        console.log('SUBCRIBING...');
        const { ok, error, subId } = await network.post('/api/subscriptions');
        const websocket = new WebSocket(`${WSS_BASE_URL}/ws/${subId}`);
        websocket.onopen = () => {
          console.log('Websocket opened...');
          websocket.send('ping');
        };

        websocket.onmessage = (msg) => {
          try {
            const payload = JSON.parse(msg.data);
            switch (payload.type) {
              case 'jam:updated': {
                dispatch({ type: ACTION_PUSH_JAM_UPDATED, jam: payload.jam });
                break;
              }
              case 'jam:created': {
                dispatch({ type: ACTION_PUSH_JAM_CREATED, jam: payload.jam });
                break;
              }
              default: break;
            }
          } catch (e) {
            console.log('inbound ws data', msg.data);
          }
        };

        setWS(websocket);

        if (ok) {
          dispatch({ type: ACTION_SUBCRIBE, status: STATUS_OK, subId });
          return;
        }
        dispatch({ type: ACTION_SUBCRIBE, status: STATUS_ERROR, error });
      } catch (error) {
        dispatch({ type: ACTION_SUBCRIBE, status: STATUS_ERROR, error });
      }})();
  }, [state.getIn(['profile', 'isLoggedIn'])]);

  return children;
};
