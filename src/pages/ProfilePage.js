/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { DispatchContext, StateContext } from "../components/context";
import { ACTION_REGISTER, ACTION_UPDATE_PROFILE, STATUS_ERROR, STATUS_PENDING } from "../constants";
import network from "../network";

export default ({ registration = true }) => {
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  const [formState, setFormState] = useState({
    username: '',
    instrument: '',
    password: '',
    // omit on submit
    confirmPassword: '',
    valid: null,
    pristine: true,
  });

  useEffect(() => {
    if (registration) { return; }

    const { username, instrument } = state.get('profile').toJS();
    setFormState( { ...formState, username, instrument, password: '', confirmPassword: '' });
  }, [state.getIn(['profile', 'isLoggedIn'])]);

  const validate = useCallback(({ username, instrument, password, confirmPassword }) => {
    if (registration) {
      return !!username &&
          !!instrument &&
          !!password &&
          !!confirmPassword &&
          password === confirmPassword
    } else {
      return !!username &&
          !!instrument &&
          (!password || (password === confirmPassword));
    }

  }, []);

  const onSaveClick = useCallback(async () => {
    if (registration) {
      const type = ACTION_REGISTER;

      dispatch({ type, status: STATUS_PENDING });
      const { username, instrument, password } = formState;
      const { ok, error } = await network.post('/api/profiles', { username, instrument, password });
      if (error) {
        dispatch({ type, status: STATUS_ERROR, error });
        return;
      } else if (ok) {
        window.location.href = '/login'; // Force page reload
      }
    } else {
      const type = ACTION_UPDATE_PROFILE;
      dispatch({ type, status: STATUS_PENDING });
      const { username, instrument, password } = formState;
      const payload = { username, instrument, password };
      if (!password) { delete payload.password; }
      const { error } = await network.patch('/api/profiles/me', payload);
      if (error) {
        dispatch({ type, status: STATUS_ERROR, error });
        return;
      }
    }
  }, [formState]);

  return (
    <div className="container">
      <h1 className="row px-3">Your Profile</h1>
      <section className="row justify-content-center">
        <div className="col-5">
          <div className="form">
            <div className="form-group">
              <label htmlFor="username" className="control-label">Name</label>
              <input
                type="text" className="form-control" id="username"
                value={formState.username}
                onChange={({ target: { value } }) => setFormState({
                  ...formState,
                  username: value,
                  valid: validate({
                    ...formState,
                    username: value,
                  }),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="instrument" className="control-label">Instrument</label>
              <select
                className="form-control" id="instrument"
                value={formState.instrument}
                onChange={({ target: { value } }) => setFormState({
                  ...formState,
                  valid: validate({
                    ...formState,
                    instrument: value,
                  }),
                  pristine: false,
                  instrument: value,
                })}
              >
                <option value="">Select your instrument</option>
                <option value="piano">Piano</option>
                <option value="tambourine">Tambourine</option>
                <option value="guitar">Guitar</option>
                <option value="violin">Violin</option>
                <option value="drum">Drum</option>
                <option value="saxophone">Saxophone</option>
                <option value="trumpet">Trumpet</option>
                <option value="clarinet">Clarinet</option>
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="password" className="control-label">Password</label>
              <input
                type="password" className="form-control" id="password"
                autoComplete="new-password"
                value={formState.password}
                onChange={({ target: { value } }) => setFormState({
                  ...formState,
                  pristine: false,
                  password: value,
                  valid: validate({
                    ...formState,
                    password: value,
                  }),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="confirmPassword" className="control-label">Confirm</label>
              <input
                type="password" className="form-control" id="confirmPassword"
                value={formState.confirmPassword}
                onChange={({ target: { value } }) => {
                  setFormState({
                    ...formState,
                    pristine: false,
                    confirmPassword: value,
                    valid: validate({
                      ...formState,
                      confirmPassword: value,
                    }),
                  });
                }}
              />
            </div>

            <button
              className="btn btn-primary"
              disabled={!formState.valid || formState.pristine}
              onClick={onSaveClick}
            >Save</button>
          </div>
        </div>
      </section>
    </div>
);
}
