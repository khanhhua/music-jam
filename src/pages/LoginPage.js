import React, { useCallback, useContext, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { DispatchContext } from "../components/context";
import { ACTION_LOGIN, STATUS_ERROR, STATUS_OK, STATUS_PENDING } from "../constants";
import network from "../network";

export default () => {
  const history = useHistory();
  const dispatch = useContext(DispatchContext);
  const [formState, setFormState] = useState({
    username: '',
    password: '',
    valid: false,
    pristine: true,
  });
  const validate = useCallback(({ username, password }) => {
    return !!username && !!password
  }, []);

  const onLoginClick = useCallback(async () => {
    const { username, password } = formState;
    dispatch({ type: ACTION_LOGIN, status: STATUS_PENDING });
    const { ok, token, error } = await network.post('/api/auth', { username, password });
    if (error) {
      dispatch({ type: ACTION_LOGIN, status: STATUS_ERROR, error });
      return;
    } else if (ok) {
      localStorage.setItem('mj:token', token);

      const { profile } = await network.get('/api/profiles/me');
      if (profile) {
        dispatch({ type: ACTION_LOGIN, status: STATUS_OK, profile });
        history.replace('/songs');
      }
      return;
    }
  }, [dispatch, history, formState]);

  return (
    <div className="container-fluid">
      <div className="row justify-content-center mt-5">
        <div className="col-5">
          <h1>Ready to Jam? Log in!</h1>
          <div className="form">
            <div className="form-group">
              <label htmlFor="username" className="control-label">Username</label>
              <input type="text" className="form-control" id="username"
                value={formState.username}
                onChange={({ target: { value } }) => {
                  setFormState({
                    ...formState,
                    username: value,
                    pristine: false,
                    valid: validate({
                      ...formState,
                      username: value,
                    })
                  })
                }}
              />
            </div>
            <div className="form-group mb-5">
              <label htmlFor="password" className="control-label">Password</label>
              <input type="password" className="form-control" id="password"
                value={formState.password}
                onChange={({ target: { value } }) => {
                  setFormState({
                    ...formState,
                    password: value,
                    pristine: false,
                    valid: validate({
                      ...formState,
                      password: value,
                    })
                  })
                }}
              />
            </div>
            <button
                className="btn btn-primary"
                disabled={!formState.valid || formState.pristine}
                onClick={onLoginClick}
            >LOGIN</button>
            <p>Not a member yet? <Link to="/register">Register now!</Link></p>
          </div>
        </div>
      </div>

    </div>
  );
}
