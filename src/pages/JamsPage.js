/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useContext, useEffect } from 'react';
import { DispatchContext, StateContext } from "../components/context";
import network from "../network";
import {
  ACTION_JOIN_JAM,
  ACTION_VIEW_JAM,
  ACTION_LIST_JAMS,
  STATUS_ERROR,
  STATUS_OK,
  STATUS_PENDING
} from "../constants";

export default () => {
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  useEffect(() => {
    (async function () {
      dispatch({ type: ACTION_LIST_JAMS, status: STATUS_PENDING });
      const { ok, jams, error } = await network.get('/api/jams');
      if (ok) {
        dispatch({ type: ACTION_LIST_JAMS, status: STATUS_OK, jams });
        return;
      }

      dispatch({ type: ACTION_LIST_JAMS, status: STATUS_ERROR, error });
    })();
  }, []);

  const onJoinJamClick = useCallback(async (jamId) => {
    const { ok, jam, error } = await network.post(`/api/jams/${jamId}/players`, {});
    if (ok) {
      dispatch({ type: ACTION_JOIN_JAM, status: STATUS_OK, jam });
      return;
    } else {
      dispatch({ type: ACTION_JOIN_JAM, status: STATUS_ERROR, error });
      return;
    }
  }, [dispatch]);

  const onViewJamClick = (jamId) => dispatch({ type: ACTION_VIEW_JAM, jamId });

  const jams = state.get('jams');

  return (
    <div className="container">
      <section className="row">
        <div className="col">
          <h1>Jams</h1>
          <ul className="list-group list-group-flush">
          {jams.map(item => (
            <li key={item.get('_id')} className="list-group-item">
              {item.get('title')}
              <span className="badge badge-pill badge-dark ml-2">
                {item.get('status')}
              </span>
              <div className="btn-group btn-group-sm song-action-group float-right">
                <button className="btn btn-info">Listen</button>
                {!(item.get('owned') || item.get('joined')) &&
                <button
                  className="btn btn-light"
                  onClick={() => onJoinJamClick(item.get('_id'))}
                >
                  Join
                </button>
                }
                <button
                  className="btn btn-light"
                  onClick={() => onViewJamClick(item.get('_id'))}
                >
                  View
                </button>
              </div>
            </li>
          ))}
          </ul>
        </div>
      </section>
    </div>
  );
};
