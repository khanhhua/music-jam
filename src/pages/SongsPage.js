/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useContext, useEffect } from 'react';
import { DispatchContext, StateContext } from "../components/context";
import network from '../network';
import {
  ACTION_CREATE_JAM,
  ACTION_LIST_SONGS,
  ACTION_VIEW_JAM,
  STATUS_ERROR,
  STATUS_OK,
  STATUS_PENDING
} from "../constants";

export default () =>{
  const dispatch = useContext(DispatchContext);
  const state = useContext(StateContext);
  useEffect(() => {
    (async function () {
      dispatch({ type: ACTION_LIST_SONGS, status: STATUS_PENDING });
      const { ok, songs, error } = await network.get('/api/songs');
      if (ok) {
        dispatch({ type: ACTION_LIST_SONGS, status: STATUS_OK, songs });
        return;
      }

      dispatch({ type: ACTION_LIST_SONGS, status: STATUS_ERROR, error });
    })();
  }, []);

  const onCreateJamClick = useCallback(async (songId) => {
    dispatch({ type: ACTION_CREATE_JAM, status: STATUS_PENDING, songId });
    const { ok, jam, error } = await network.post('/api/jams', { songId });
    if (ok) {
      dispatch({ type: ACTION_CREATE_JAM, status: STATUS_OK, jam });
      dispatch({ type: ACTION_VIEW_JAM, jamId: jam._id });
    } else {
      dispatch({ type: ACTION_CREATE_JAM, status: STATUS_ERROR, error });
    }
  }, [dispatch]);

  const songs = state.get('songs');

  return (
    <div className="container">
      <section className="row">
        <div className="col">
          <h1>Songs</h1>
          <ul className="list-group list-group-flush">
          {songs.map(item => (
            <li key={item.get('_id')} className="list-group-item">
              {item.get('title')}
              <div className="btn-group btn-group-sm song-action-group float-right">
                <button className="btn btn-info">Listen</button>
                <button
                  className="btn btn-light"
                  onClick={() => onCreateJamClick(item.get('_id'))}
                >
                  Jam
                </button>
              </div>
            </li>
          ))}
          </ul>
        </div>
      </section>
    </div>
  );
};
