import React, { useReducer } from 'react';
import { fromJS } from 'immutable';
import 'bootstrap/dist/css/bootstrap.css';
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import './App.scss';

import { StateContext, DispatchContext } from './components/context';
import LoginPage from './pages/LoginPage';
import SongsPage from './pages/SongsPage';
import InteractiveJam from './components/InteractiveJam';

import JamsPage from './pages/JamsPage';
import reducer from './components/reducer';
import ProfilePage from "./pages/ProfilePage";
import ErrorRollout from "./components/ErrorRollout";
import SessionLayer from "./components/SessionLayer";
import AppNav from "./components/AppNav";

const initialApplicationData = fromJS({
  errorText: null,
  profile: {
    isLoggedIn: false,
    name: null,
    instrument: null,
  },
  notifications: [],
  songs: [],
  jams: [],
  activeJamId: null, // a jam being in the foreground
});

function App() {
  const [state, dispatch] = useReducer(reducer, initialApplicationData);

  return (
    <DispatchContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>
        <ErrorRollout errorText={state.get('errorText')} visible={!!state.get('errorText')} />
        <Router>
          <div className="App">
            <Switch>
              <Route exact path="/login">
                <LoginPage />
              </Route>
              <Route exact path="/register">
                <ProfilePage />
              </Route>
              <Route exact path="/">
                <Redirect to={{ pathname: '/login' }} />
              </Route>
              {/* -- Protected --> */}
              <Route render={(props) => {
                const tokenExists = !!localStorage.getItem('mj:token');
                if (!(state.getIn(['profile', 'isLoggedIn']) || tokenExists)) {
                  return (<Redirect to={{ pathname: '/login' }} />);
                }

                return (
                  <SessionLayer>
                    <AppNav profile={state.get('profile')} />
                    <Switch>
                      <Route exact path="/songs">
                        <SongsPage />
                      </Route>
                      <Route exact path="/jams">
                        <JamsPage />
                      </Route>
                      <Route exact path="/profile">
                        <ProfilePage registration={false} />
                      </Route>
                      <Route>
                        <Redirect to={{ pathname: '/songs' }} />
                      </Route>
                    </Switch>
                  </SessionLayer>
                );
              }} />
            </Switch>
          </div>
        </Router>
        {!!state.get('activeJamId') &&
        <InteractiveJam jamId={state.get('activeJamId')} />
        }
      </StateContext.Provider>
    </DispatchContext.Provider>
  );
}

export default App;
